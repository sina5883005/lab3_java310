package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void returnTestEcho(){
        App ap = new App();
        assertEquals("Checking If echo method IN App working the way it should", 5, ap.echo(5));
    }

    @Test
    public void returnTestOneMore(){
        App ap = new App();
        assertEquals("Checlin if oneMore metohd returning the right value as it should", 6, ap.oneMore(5));
    }
}
